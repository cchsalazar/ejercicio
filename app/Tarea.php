<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $table = 'tareas';
    protected $fillable = array('id', 'tareaTitulo', 'tareaCuerpo', 'tareaCompletada');
    public $timestamps = false;
}
