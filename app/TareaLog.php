<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tarea;

class TareaLog extends Model
{
    protected $table = 'tareas_log';
    protected $fillable = array('id', 'tareaTitulo', 'tareaCuerpo', 'tareaCompletada', 'fechaActualizado', 'borrado');
    public $timestamps = false;
}
