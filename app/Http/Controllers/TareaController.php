<?php

namespace App\Http\Controllers;

use App\Tarea;
use App\TareaLog;
use Carbon\Carbon;
use Faker\Provider\zh_TW\DateTime;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;

class TareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if ($id == null) {
            return Tarea::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $tarea_saved = false;
        $log_saved = false;

        DB::beginTransaction();

        try {

            $tarea = new Tarea();
            $tarea->tareaTitulo = $request->input('tareaTitulo');
            $tarea->tareaCuerpo = $request->input('tareaCuerpo');
            $tarea->tareaCompletada = (boolean) $request->input('tareaCompletada');
            $tarea_saved = $tarea->save();

            $log = new TareaLog();
            $log->id = $tarea->id;
            $log->tareaTitulo = $request->input('tareaTitulo');
            $log->tareaCuerpo = $request->input('tareaCuerpo');
            $log->tareaCompletada = (boolean) $request->input('tareaCompletada');
            $log->fechaActualizado = Carbon::now();
            $log->borrado = 0;
            $log_saved = $log->save();

            DB::commit();

        } catch (\Exception $e) {

            DB::rollback();

        }

        //echo "tarea_saved: {$tarea_saved}, log_saved: {$log_saved}";

        //todo: implementar manejo de excepciones, respuesta con estados y routing de vistas front (no location.reload)

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Tarea::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tarea = Tarea::find($id);
        $tarea->tareaTitulo = $request->input('tareaTitulo');
        $tarea->tareaCuerpo = $request->input('tareaCuerpo');
        $tarea->tareaCompletada = (boolean) $request->input('tareaCompletada');
        $tarea->save();

        $log = new TareaLog();
        $log->id = $tarea->id;
        $log->tareaTitulo = $request->input('tareaTitulo');
        $log->tareaCuerpo = $request->input('tareaCuerpo');
        $log->tareaCompletada = (boolean) $request->input('tareaCompletada');
        $log->fechaActualizado = Carbon::now();
        $log->borrado = 0;
        $log->save();

        //todo: implementar manejo de excepciones, respuesta con estados y routing de vistas front (no location.reload)
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tarea = Tarea::find($id);
        $deleted = $tarea->delete();

        $log = new TareaLog();
        $log->id = $tarea->id;
        $log->tareaTitulo = $tarea->tareaTitulo;
        $log->tareaCuerpo = $tarea->tareaCuerpo;
        $log->tareaCompletada = $tarea->tareaCompletada;
        $log->fechaActualizado = Carbon::now();
        $log->borrado = 1;
        $log->save();

        //todo: implementar manejo de excepciones, respuesta con estados y routing de vistas front (no location.reload)
    }
}

