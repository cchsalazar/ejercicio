app.controller('TareaController', function($scope, $http, API_URL) {

    // traer lista de Tareas desde API
    $http.get(API_URL + 'tarea')
        .success(function(response){
            $scope.tareas = response;
        });

    // mostrar formulario modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;
        switch(modalstate) {
            case 'add':
                $scope.formulario_titulo = "Agregar Tarea";
                $scope.tarea = {};
                $scope.tarea.tareaCompletada = 0;
                break;
            case 'edit':
                $scope.formulario_titulo = "Editar Tarea";
                $scope.id = id;
                $http.get(API_URL + 'tarea/' + id).success(function(response){
                    console.log(response);
                    $scope.tarea = response;
                    $scope.tarea.tareaCompletada = parseInt(response.tareaCompletada);
                });
                break;
            default:
                break;
        }
        console.log(id);
        $('#myModal').modal('show');
    };

    // guardar nueva tarea y actualizar tarea existente
    $scope.save = function(modalstate, id) {
        var url = API_URL + "tarea";
        if (modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: (!id ? 'POST' : 'PUT'),
            url: url,
            data: $.param($scope.tarea),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
            console.log(response);
            location.reload();
        }).error(function(response){
            console.log(response);
            alert('Esto es embarazoso. Ocurrió un error. Visualice el log para más detalles.');
        });
    };

    // eliminar tarea
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Está seguro que desea eliminar el registro?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'tarea/' + id
            }).success(function(data){
                console.log(data);
                location.reload();
            }).error(function(data){
                console.log(data);
                alert('No se pudo eliminar');
            });
        } else {
            return false;
        }
    };

});