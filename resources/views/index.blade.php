<!DOCTYPE html>
<html lang="en" ng-app="getTarea">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Ejercicio - Tareas</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <div class="container">
        <h2>Tareas</h2>
        <div ng-controller="TareaController">
            <table class="table table-striped" id="tabla-lista">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Cuerpo</th>
                    <th>Compl.</th>
                    <th>
                        <button id="btn-add" class="btn btn-success btn-xs" ng-click="toggle('add', 0)">Agregar Tarea</button>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="tarea in tareas">
                    <td>@{{ tarea.id }}</td>
                    <td>@{{ tarea.tareaTitulo }}</td>
                    <td>@{{ tarea.tareaCuerpo }}</td>
                    <td>@{{ tarea.tareaCompletada == "1" ? "si" : "no" }}</td>
                    <td>
                        <button class="btn btn-warning btn-xs btn-detail" ng-click="toggle('edit', tarea.id)">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                        <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(tarea.id)">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- show modal  -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">@{{formulario_titulo}}</h4>
                        </div>
                        <div class="modal-body">
                            <form name="frmTarea" class="form-horizontal" novalidate="">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Título</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="tareaTitulo" name="tareaTitulo" placeholder="Título" value="@{{tareaTitulo}}" ng-model="tarea.tareaTitulo" ng-required="true">
                                        <span ng-show="frmTarea.tareaTitulo.$invalid && frmTarea.tareaTitulo.$touched">Título es requerido</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Cuerpo</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" class="form-control" id="tareaCuerpo" name="tareaCuerpo" placeholder="Tarea Cuerpo" value="@{{tareaCuerpo}}" ng-model="tarea.tareaCuerpo" ng-required="true"> -->
                                        <textarea class="form-control" id="tareaCuerpo" name="tareaCuerpo" placeholder="Cuerpo" ng-model="tarea.tareaCuerpo" ng-required="true" rows="3">@{{tareaCuerpo}}</textarea>
                                        <span ng-show="frmTarea.tareaCuerpo.$invalid && frmTarea.tareaCuerpo.$touched">Cuerpo es requerido</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">&nbsp;</label>
                                    <div class="col-sm-9">

                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="tareaCompletada" name="tareaCompletada" placeholder="Tarea Completada"
                                                        ng-model="tarea.tareaCompletada"
                                                        ng-true-value="1" ng-false-value="0"> Completada
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmTarea.$invalid">Guardar Cambios</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- Aangular Material load from CDN -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.1/angular-material.min.js"></script>

    <!-- Angular Application Scripts Load  -->
    <script src="{{ asset('angular/app.js') }}"></script>
    <script src="{{ asset('angular/controllers/TareaController.js') }}"></script>

</body>
</html>