<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tareas_log', function(Blueprint $table) {
            $table->integer('id');
            $table->string('tareaTitulo');
            $table->text('tareaCuerpo');
            $table->boolean('tareaCompletada')->default(false);
            $table->timestamp('fechaActualizado');
            $table->boolean('borrado');
            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tareas_log');
    }
}
